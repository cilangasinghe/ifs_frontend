/*global angular*/
(function () {
    'use strict';
    var surveyAppServices = angular.module('surveyApp.services');
    surveyAppServices.service('employeeService', ['$resource', '$q', function ($resource, $q) {
        var baseUri = "http://localhost:40";
        return {
            addFeedback: function (userData, feedbackToBeSaved, date) {
                var feedbackAddingUri = baseUri + '/api/Feedback';
                var employeeResource = $resource(feedbackAddingUri, {}, {
                    post: {
                        method: 'POST'
                        , isArray: false
                    }
                });
                

                var dataObject = {
                    FeedbackCaseId: userData.caseId
                    , FeedbackTaskId: userData.taskId
                    , FeedbackUserId: userData.userId
                    , FeedbackOwnerId: userData.ownerId
                    , FeedbackRecDate: date
                    , FeedBackModifiedBy: null
                    , FeedbackModifiedDate: null
                    , FeedbackComment: feedbackToBeSaved.Comment
                    , FeedbackAnalyticalSkills: feedbackToBeSaved.AnalyticalSkills
                    , FeedbackCommunication: feedbackToBeSaved.Communication
                    , FeedbackQualityTime: feedbackToBeSaved.QualityTime
                };
                var defferedObject = $q.defer();
                employeeResource.post(JSON.stringify(dataObject), function (response) {
                    defferedObject.resolve(response);
                }, function (responseStatus) {
                    defferedObject.reject(responseStatus);
                });
                return defferedObject.promise;
            }
            , getFeedbacks: function () {
                var feedbacksUri = baseUri + '/api/Feedback';
                var employeeResource = $resource(feedbacksUri, {}, {
                    get: {
                        method: 'GET'
                        , isArray: true
                    }
                });
                var deferedObject = $q.defer();
                employeeResource.get({}, function (responseData) {
                    deferedObject.resolve(responseData);
                }, function (response) {
                    deferedObject.reject(response);
                });
                return deferedObject.promise;
            }
        };
    }]);
}());