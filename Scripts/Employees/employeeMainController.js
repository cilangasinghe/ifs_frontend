/*global angular*/
(function () {
    'use strict';

    var surveyAppControllers = angular.module('surverApp.controllers');


    surveyAppControllers.controller('employeeMainController', ['$scope', '$state', function ($scope, $state) {
        $scope.employeePageStates = {
            Home: "Home",
            Add: "Add",
            List: "List"
        };

        $scope.employeePageCurrentState = $scope.employeePageStates.Home;

        $scope.swichEmployeePageState = function (pageState) {
            $scope.employeePageCurrentState = pageState;
            switch ($scope.employeePageCurrentState) {
            case $scope.employeePageStates.Home:
                $state.go('Home');
                break;
            case $scope.employeePageStates.Add:
                $state.go('Add');
                break;
            case $scope.employeePageStates.List:
                $state.go('List');
                break;
            }
        };

        function loadData() {
            $scope.swichEmployeePageState($scope.employeePageStates.Home);
        }

        loadData();
    }]);

}());