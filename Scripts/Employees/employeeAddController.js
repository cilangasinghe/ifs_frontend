/*global angular,$*/
(function () {
    'use strict';
    var surveyAppControllers = angular.module('surverApp.controllers');
    surveyAppControllers.controller('employeeAddController', ['$scope', 'employeeService', '$stateParams', function ($scope, employeeService, $stateParams) {
        $scope.case_id = null;
        $scope.current_date = null;
        $scope.owner_id = null;
        $scope.task_id = null;
        $scope.saveFeedback = function () {
            var promiseForSave = employeeService.addFeedback($scope.userData, $scope.feedback, $scope.currentDate);
            promiseForSave.then(function (responseData) {
                $("#success_modal").modal('show');
            }, function (response) {
                window.alert(JSON.stringify(response));
            });
        };
       $scope.cancelFeedback = function(){
           $scope.feedback.AnalyticalSkills = false;
           $scope.feedback.QualityTime = false;
           $scope.feedback.Communication =  false;
           $scope.feedback.Comment = null;
       };
        
        $('#success_modal').on('hidden.bs.modal', function () {
            location.reload();
        });
        
        
        function loadData() {
            $scope.userData = {};
            $scope.currentDate = new Date(),'yyyy-MM-dd';
            
            var caseId = $stateParams.case_id;
            $scope.userData.caseId = caseId;
            var ownerId = $stateParams.owner_id;
            $scope.userData.ownerId = ownerId;
            var userId = $stateParams.user_id;
            $scope.userData.userId = userId;
            var taskId = $stateParams.task_id;
            $scope.userData.taskId = taskId;
        };
        loadData();
    }]);
}());