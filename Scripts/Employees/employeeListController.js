/*global angular*/

(function () {
    'use strict';

    var surveyAppControllers = angular.module('surverApp.controllers');

    surveyAppControllers.controller('employeeListController', ['$scope', 'feedbackService', function ($scope, feedbackService) {
        $scope.feedbacks = null;

        function loadData() {
            var promiseForEmployees = feedbackService.getFeedbacks();

            promiseForEmployees.then(function (responseData) {
                $scope.feedbacks = responseData;
            }, function (responseStatus) {
                window.alert(JSON.stringify(responseStatus));

            });
        }

        loadData();
    }]);

}());