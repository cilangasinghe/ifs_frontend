/*global angular*/
(function () {
    'use strict';

    var surveyAppServices = angular.module('surveyApp.services', []),
        surveyAppControllers = angular.module('surverApp.controllers', []),
        SurveyAppModule = angular.module('surverApp', ['ui.router', 'surverApp.controllers', 'surveyApp.services', 'ngResource']);



    SurveyAppModule.config(function ($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/');

        $stateProvider.state('Home', {
            url: '/home',
            templateUrl: '/Partials/Employees/employee-home.html',
            controller: 'employeeHomeController'
        }).state('Add', {
            url: '/add?case_id&task_id&user_id&owner_id',
            templateUrl: '/Partials/Employees/employee-add.html',
            controller: 'employeeAddController'
        }).state('List', {
            url: '/list',
            templateUrl: '/Partials/Employees/employee-list.html',
            controller: 'employeeListController'
        });

    });

}());